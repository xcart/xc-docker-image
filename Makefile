NAME := xc-docker-image
VERSION := 7.2-nginx
DOCKER_REGISTRY := xcart

build:
	docker build . -t $(DOCKER_REGISTRY)/$(NAME):$(VERSION)

pull:
	docker pull $(DOCKER_REGISTRY)/$(NAME):$(VERSION)

clean:
	docker rmi -f $(DOCKER_REGISTRY)/$(NAME):$(VERSION)

release:
	docker tag $(DOCKER_REGISTRY)/$(NAME):$(VERSION) $(DOCKER_REGISTRY)/$(NAME):latest
	docker push $(DOCKER_REGISTRY)/$(NAME):latest

.PHONY: build pull clean release