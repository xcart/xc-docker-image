# X-Cart 5 in Docker

This repo contains base image for X-Cart 5.3.4+ stores in both CI and development-ready formats.

## Use in your project (development)

To run X-Cart in Docker for development, you should copy a set of files from `template` folder to your local project webroot directory:

```
.dockerignore
docker-compose.ci.yml
docker-compose.dev.yml
docker-compose.yml
docker-sync.yml
Dockerfile
```

This setup implies that you have both [Docker](https://www.docker.com/products/docker-desktop) and [docker-sync](https://docker-sync.readthedocs.io/en/latest/getting-started/installation.html) apps installed. If not, install Docker from official website and then run the following command:

```
gem install docker-sync
```

Ok, you're almost there! You just have to prepare your `etc/config.php` file properly. The default setup for an app on `localhost` is the following:

```
; <?php /*
; WARNING: Do not change the line above

[database_details]
hostspec = "127.0.0.1"
database = "xc5" # this corresponds to DATABASE_NAME in docker-compose.yml
username = "xc5" # this corresponds to DATABASE_USER in docker-compose.yml
password = "123456" # this corresponds to DATABASE_Pass in docker-compose.yml
table_prefix = "xc_"

[host_details]
http_host = "localhost:8080" # this corresponds to DOMAIN:NGINX_PORT in docker-compose.yml
https_host = "localhost:8443" # this corresponds to DOMAIN:NGINX_SSL_PORT in docker-compose.yml
web_dir = ""

[service]
verify_certificate = Off

; WARNING: Do not change the line below
; */ ?>
```

Be sure that the following settings are exactly as in your docker-compose.yml file.

Now to finally start the application, run the following command:

```
docker-sync-stack start
```

It would start sync and compose files with the app, lock a shell for a log output. If you input Ctrl-C, it would gracefully stop both the docker-sync and docker-compose, leaving the volumes intact. First run might be slow - that's ok. Consequent runs would be much faster.

Once you see that the app is up, try accessing your website: http://localhost:8080 or https://localhost:8443.

If you want to start over from scratch, simply run this:

```
docker-sync-stack clean
```

This command will remove containers and their volumes.

## SSL setup

We need to install and use [mkcert](https://github.com/FiloSottile/mkcert) tool to get proper SSL certificates. This tool add a local CA root to your system and helps to issue certificates. Run these commands to install the tool once:

```
brew install mkcert
mkcert -install
```

After that, run the following command to generate certificate-key pair for a certain set of domains:

```
mkcert "*.localhost" localhost 127.0.0.1 ::1
```

As a result, `mkcert` will output two files: private key (something like "_wildcard.localhost+3-key.pem") and certificate file ("_wildcard.localhost+3.pem"). Rename them to `localhost.key` and `localhost.crt`, respectively and put them near docker-compose.yml file. You should copy these files for each local installation that goes by `localhost` domain.

One last step - open your `docker-compose-dev.yml` and uncomment volume strings that copy certificate and key to the container. That's it - website should support SSL now.