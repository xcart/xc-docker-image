FROM php:7.2-fpm

# supervisord & tools
RUN apt-get update -y && apt-get install -y supervisor zip unzip
RUN mkdir -p /var/log/supervisor

# MariaDb configuration
RUN apt-get update -y \
    && apt-get install -y software-properties-common dirmngr \
    && apt-get install -y gnupg curl vim git gettext-base
RUN apt-key adv --fetch-keys 'https://mariadb.org/mariadb_release_signing_key.asc'
RUN add-apt-repository 'deb [arch=amd64,i386] http://mirror.truenetwork.ru/mariadb/repo/10.4/debian buster main'

RUN apt-get update -y \
    && apt-get install -y mariadb-server mariadb-client

# Nginx configuration
RUN apt-get install -y nginx
COPY nginx-site.conf /etc/nginx/site-template.conf

# PHP_CPPFLAGS are used by the docker-php-ext-* scripts
ENV PHP_CPPFLAGS="$PHP_CPPFLAGS -std=c++11"

# PHP configuration
RUN docker-php-ext-install pdo_mysql \
    && docker-php-ext-install opcache \
    && apt-get install libicu-dev -y \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl \
    && apt-get remove libicu-dev icu-devtools -y \
    && docker-php-ext-install mbstring

COPY ./php-opcache-cfg.ini /usr/local/etc/php/conf.d/php-opcache-cfg.ini

RUN apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        zlib1g-dev \
    && docker-php-ext-configure gd --with-gd --with-jpeg-dir \
       --with-png-dir --with-zlib-dir --with-freetype-dir \
    && docker-php-ext-install -j$(nproc) gd

RUN mkdir -p /var/run/php && mkdir -p /var/run/mysqld \
    && chown mysql:root /var/run/mysqld

ENV DOMAIN=127.0.0.1
ENV DATABASE_NAME=xc5
ENV DATABASE_USER=xc5
ENV DATABASE_PASS=123456
ENV NGINX_PORT=8080
ENV NGINX_SSL_PORT=8443
ENV SSL_CERT=/etc/nginx/ssl/localhost.crt
ENV SSL_KEY=/etc/nginx/ssl/localhost.key
ENV SEED_PATH=/var/www/html/seed.sql

COPY ./supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY ./tools.phar /usr/local/sbin/tools
COPY ./init.sh /usr/local/sbin/init.sh
COPY ./docker_entrypoint.sh /usr/local/sbin/docker_entrypoint.sh

WORKDIR /var/www/html

EXPOSE 8080 8443

ENTRYPOINT ["/usr/local/sbin/docker_entrypoint.sh"]
CMD ["/usr/bin/supervisord"]