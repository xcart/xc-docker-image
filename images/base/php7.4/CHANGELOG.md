# Project changelog

### v1.0.7
Author: Максим Шамаев <max@x-cart.com>
* fix cron

### v1.0.6
Author: Максим Шамаев <max@x-cart.com>
* remove usermod

### v1.0.5
Author: Максим Шамаев <max@x-cart.com>
* add j2cli

### v1.0.4
Author: Максим Шамаев <max@x-cart.com>
* add shell_templater

### v1.0.3
Author: Максим Шамаев <max@x-cart.com>
* update composer to 2

### v1.0.2
Author: Максим Шамаев <max@x-cart.com>
* network-check improved

### v1.0.1
Author: Максим Шамаев <max@x-cart.com>
* yarn added

### v1.0.0
Author: Максим Шамаев <max@x-cart.com>
* initial version
