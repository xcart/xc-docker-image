#!/usr/bin/env sh
set -eu

for i in `find /etc/nginx/sites-templates/*.j2`; do
  o=`echo $i | sed 's/sites-templates/sites-enabled/g' | sed 's/\.j2//g'`
  echo "$i in $o";
  j2 -f env $i > $o
done

# example: PREPARE_SCRIPT_USER=www-data PREPARE_SCRIPT="/opt/app/bin/console cache:warmup --no-debug"
if [ ! -z "${PREPARE_SCRIPT:-}" ]; then
    if [ ! -z "${PREPARE_SCRIPT_USER:-}" ] && [ `whoami` != "$PREPARE_SCRIPT_USER" ]; then
        sudo -E -u "$PREPARE_SCRIPT_USER" $PREPARE_SCRIPT
    else
        $PREPARE_SCRIPT
    fi
fi

case "$1" in
    nginx)
        exec nginx -g 'daemon off;'
        ;;

    *)
        exec "$@"
        ;;
esac
