# Project changelog

### v1.0.5
Author: Максим Шамаев <max@x-cart.com>
* add symfony_fastcgi

### v1.0.4
Author: Максим Шамаев <max@x-cart.com>
* add templates autoprocessing

### v1.0.3
Author: Максим Шамаев <max@x-cart.com>
* add j2cli

### v1.0.2
Author: Максим Шамаев <max@x-cart.com>
* add shell_templater

### v1.0.1
Author: Максим Шамаев <max@x-cart.com>
* add include/symfony_base.conf

### v1.0.0
Author: Максим Шамаев <max@x-cart.com>
* initial version
