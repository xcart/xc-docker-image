# Project changelog

### v1.0.1
Author: Максим Шамаев <max@x-cart.com>
* add healthcheck

### v1.0.0
Author: Максим Шамаев <max@x-cart.com>
* initial version
