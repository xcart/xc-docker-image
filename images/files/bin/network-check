#!/usr/bin/env php
<?php
# выполняет проверку готовности fpm сервиса (readiness probe)
$shortopts = '';
$longopts = [
    'nginx-host:', // Обязательное значение: nginx домен
    'nginx-uri::', // Обязательное значение: URI по которому будет делаться чек
];
$options = getopt($shortopts, $longopts);

# первая проверка: сетевой сокет открыт
if (@fsockopen("localhost:9000")) {
    echo "* Socket check passed\n";
} else {
    echo "* Socket check failed (network socket is closed for localhost:9000)\n";
    exit(1);
}

if (!isset($options['nginx-host'])) {
    die(0);
}

# вторая проверка: nginx контейнеры доступны по сети
global $nginxHostname;
$nginxHostname = $options['nginx-host'];
global $nginxHttpCheckUri;
$nginxHttpCheckUri = $options['nginx-uri'] ?? '/_/healthcheck/nginx';

try {
    foreach (getNginxARecords() as $record) {
        $checkUrl = getHostCheckUri($record['ip']);
        $httpCode = 0;
        echo "Checking $checkUrl ... ";
        if (curlCheck($checkUrl, $httpCode)) {
            echo "ok.\n";
        } else {
            throw new Exception(
                sprintf('%s / %s is not accessible. HTTP code: %s', $nginxHostname, $checkUrl, $httpCode)
            );
        }
    }

    echo "* Nginx connection passed\n";
} catch (Throwable $e) {
    echo "* Nginx connection failed ({$e->getMessage()})\n";
    exit(1);
}

function curlCheck(string $url, int &$httpCode): bool
{
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 1);
    curl_exec($ch);
    $httpCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    return $httpCode === 200;
}

function getNginxARecords(): array
{
    global $nginxHostname;

    $records = dns_get_record($nginxHostname, DNS_A);

    if (empty($records)) {
        throw new Exception("{$nginxHostname} has no DNS A record");
    }

    return $records;
}

function getHostCheckUri(string $host): string
{
    global $nginxHttpCheckUri;

    return "http://{$host}{$nginxHttpCheckUri}";
}
