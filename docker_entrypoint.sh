#!/usr/bin/env sh
set -eu

# creates nginx config from template
envsubst '${NGINX_PORT}' < /etc/nginx/site-template.conf > /etc/nginx/sites-enabled/default

# enables ssl config if certificates are present
if [ \( -f "$SSL_CERT" \) -a \( -f "$SSL_KEY" \) ]; then
    sed -i "3a\listen $NGINX_SSL_PORT ssl;" /etc/nginx/sites-enabled/default
    sed -i "4a\ssl_certificate $SSL_CERT;" /etc/nginx/sites-enabled/default
    sed -i "5a\ssl_certificate_key $SSL_KEY;" /etc/nginx/sites-enabled/default
fi

# chowns files in webroot
chown -R www-data:www-data /var/www/html

# runs anything passed in CMD
exec "$@"