#!/usr/bin/env bash
set -e

DIR=$1
BUILD_PATH=$DIR

# We expect in $DIR/conf
# these vars:
# -IMAGE
# -TAG
# and maybe overriding of:
# -BUILD_PATH
source $DIR/conf

echo "> Building $IMAGE:$TAG ..."
docker build -t $IMAGE:$TAG -f $DIR/Dockerfile $BUILD_PATH
docker image tag $IMAGE:$TAG $IMAGE:latest
if [[ "$2" == "--no-push" ]]; then
    echo "Not pushed."
else
    docker push $IMAGE:$TAG
    docker push $IMAGE:latest
fi
