#!/bin/bash

while ! mysqladmin ping -h "127.0.0.1" --silent; do
    echo -e "MySQL server at 127.0.0.1 not ready, trying again later ..."
    sleep 1
done
echo 'MySQL started, running DB init script.'

if [ ! -d "/var/lib/mysql/$1" ] ; then 
  echo "Creating database $1 and user $2 ..."

  mysql -uroot <<MYSQL_SCRIPT
CREATE DATABASE $1 CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER '$2'@'localhost' IDENTIFIED BY '$3';
GRANT ALL PRIVILEGES ON $1.* TO '$2'@'localhost';
FLUSH PRIVILEGES;
MYSQL_SCRIPT
fi

SEED=$4
if [ -f "$SEED" ]; then
  if [ "$(mysqlshow $1 | wc -l)" -lt 6 ] ; then 
    echo "Loading dump file $SEED ..."
    mysql -uroot <<MYSQL_SCRIPT
USE $1;
SET FOREIGN_KEY_CHECKS=OFF;
SOURCE $SEED
SET FOREIGN_KEY_CHECKS=ON;
MYSQL_SCRIPT
    echo "Dump file $SEED loaded."
  else
    echo "Database is not empty, dump file not loaded. To clear, run: docker-compose down -v."
  fi
else
  echo "Dump file $SEED not found."
fi

AUTHCODE=$(grep -rn auth_code /var/www/html/etc)
echo "Now you should redeploy X-Cart to start working. Auth code is one of the following: "
echo "$AUTHCODE"

# echo "Running redeploy"
# cd /var/www/html
# tools redeploy